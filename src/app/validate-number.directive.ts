import {AbstractControl, NG_VALIDATORS, ValidationErrors, Validator} from "@angular/forms";
import {Directive} from "@angular/core";

export const  phoneValidator= (control: AbstractControl): ValidationErrors | null => {

    const hasNumber = /\b055|077|099\d{6}\b/.test(control.value);

    if(hasNumber){
      return null;
    }

    return {phoneNumber : true}

};

@Directive({
  selector :'[appPhone]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: ValidateNumberDirective,
    multi: true
  }]
})
export class ValidateNumberDirective implements Validator{
  validate(control: AbstractControl): ValidationErrors | null {
    return phoneValidator(control);
  }

}
