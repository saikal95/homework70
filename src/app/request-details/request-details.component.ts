import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {RequestService} from "../shared/request.service";
import {Request} from "../shared/request.model";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-request-details',
  templateUrl: './request-details.component.html',
  styleUrls: ['./request-details.component.css']
})
export class RequestDetailsComponent implements OnInit {

  request!: Request;
  requestSubscription! : Subscription;
  isRemoving = false;
  requestRemovingSubscription !: Subscription;


  constructor(private route: ActivatedRoute, private requestService: RequestService,
              private router: Router) { }

  ngOnInit(): void {
   this.route.data.subscribe(data=> {
     this.request = <Request>data.request;

     this.requestRemovingSubscription = this.requestService.requestRemoving.subscribe((isRemoving: boolean) => {
       this.isRemoving = isRemoving;
     })


   })
  }

  deleteRequest() {
    this.requestService.removeRequest(this.request.id).subscribe(()=> {
      this.requestService.fetchRequests();
      void this.router.navigate(['/requests']);
    });
  }


  // ngOnDestroy() {
  //   this.requestSubscription.unsubscribe();
  // }


}
