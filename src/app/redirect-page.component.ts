import {Component} from "@angular/core";

@Component({
  selector: 'app-redirect-page',
  template:`<h1>Ваша регистрация принята спасибо! </h1>`,
  styles: [`
    h1{
      color: dodgerblue;
      font-style: italic;
    }
  `]
})

export class RedirectPageComponent {}
