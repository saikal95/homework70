export class Request {
  constructor(
    public id: string,
    public name: string,
    public surName: string,
    public middleName: string,
    public phoneNumber: string,
    public work: string,
    public gender: string,
    public select: string,
    public comment: string,
    public skills:string,
  ) {}
}
