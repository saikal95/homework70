import {Subject} from "rxjs";
import {Request} from "./request.model";
import {HttpClient} from "@angular/common/http";
import {map, tap} from "rxjs/operators";
import {Injectable} from "@angular/core";
import {createLogErrorHandler} from "@angular/compiler-cli/ngcc/src/execution/tasks/completion";

@Injectable()

export  class RequestService {
  requestChange = new Subject <Request[]>();
  requestFetching = new Subject<boolean>();
  requestRemoving = new Subject <boolean>();


  private requests: Request[] = [
  ];

  constructor(private http: HttpClient) {}


  sendRequest(body: Object){
    this.http.post('https://another-plovo-default-rtdb.firebaseio.com/requests.json', body).subscribe();

  }

  fetchRequests(){
    this.requestFetching.next(true);
    this.http.get<{[id:string]:Request}>('https://another-plovo-default-rtdb.firebaseio.com/requests.json')
      .pipe(map (result => {
        return Object.keys(result).map(id=> {
          const requestData = result[id];
          return new Request(
             id,
            requestData.name,
            requestData.surName,
            requestData.middleName,
            requestData.phoneNumber,
            requestData.work,
            requestData.gender,
            requestData.select,
            requestData.comment,
            requestData.skills
          );
        });
      }))
      .subscribe(requests=>{
        this.requests = requests;
        this.requestChange.next(this.requests.slice());
        this.requestFetching.next(false);
      }, () => {
        this.requestFetching.next(false);
      })
  }


  editRequest(request: Request){
    const body = {
      name: request.name,
      surName: request.surName,
      middleName: request.middleName,
      phoneNumber: request.phoneNumber,
      work: request.work,
      gender: request.gender,
      select: request.select,
      comment: request.comment
    }

    return this.http.put(`https://another-plovo-default-rtdb.firebaseio.com/requests/${request.id}.json`, body).pipe(
      tap());
  }



  fetchRequest(id: string) {
    return this.http.get<Request | null>(`https://another-plovo-default-rtdb.firebaseio.com/requests/${id}.json`).pipe(

      map(result => {
        if(!result)return null;

        return new Request(
          id, result.name, result.surName, result.middleName,
          result.phoneNumber, result.work, result.gender,
          result.select, result.comment, result.skills,
        );
      })
    );
  }


  removeRequest(id: string) {
    this.requestRemoving.next(true);

    return this.http.delete(`https://another-plovo-default-rtdb.firebaseio.com/requests/${id}.json`).pipe(
      tap(() => {
        this.requestRemoving.next(false);
      }, () => {
        this.requestRemoving.next(false);
      })
    );
  }


}
