import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from "@angular/router";
import {RequestService} from "./shared/request.service";
import {EMPTY, Observable, of} from "rxjs";
import {Request} from "./shared/request.model";
import {mergeMap} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class RequestResolverService implements Resolve <Request> {

  constructor(private requestService: RequestService, private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Request> | Observable<never> {
    const requestId = <string>route.params['id'];
    return this.requestService.fetchRequest(requestId).pipe(mergeMap(request => {
      if (request) {
        return of(request);
      } else {
        void this.router.navigate(['/requests']);
        return EMPTY;
      }
    }))
  }
}

