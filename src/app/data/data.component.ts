import {Component, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";
import {phoneValidator} from "../validate-number.directive";
import {RequestService} from "../shared/request.service";
import {Request} from "../shared/request.model";

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit {

  userForm!: FormGroup;
  isEdit= false;
  editedId ='';

  constructor(private http: HttpClient,
              private requestService: RequestService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.userForm = new FormGroup({
      name: new FormControl('', Validators.required),
      surName: new FormControl('', Validators.required),
      middleName: new FormControl('', Validators.required),
      phoneNumber: new FormControl('', [
        Validators.required, Validators.maxLength(10),
        phoneValidator
      ]),
      work: new FormControl('', Validators.required),
      gender: new FormControl('', Validators.required),
      select: new FormControl('', Validators.required),
      comment: new FormControl('', [Validators.required, Validators.maxLength(300)]),
      skills: new FormArray([], Validators.required)
    })

    this.route.data.subscribe (data => {
      const request = <Request | null> data.request;
      if (request) {
        this.isEdit = true;
        this.editedId = request.id;
         this.setFormValue({
           name: request.name,
           surName: request.surName,
           middleName: request.middleName,
           phoneNumber: request.phoneNumber,
           work: request.work,
           gender: request.gender,
           select: request.gender,
           comment: request.comment,
           skills: request.skills
         })
      } else {
        this.isEdit = false;
        this.editedId = '';
       }
      })
  }

  setFormValue(value: {[key: string]: any}) {
    setTimeout(() => {
      this.userForm.setValue(value);
    });
  }

  saveRequest() {
    const request = new Request(
      this.userForm.value.id,
      this.userForm.value.name,
      this.userForm.value.surName,
      this.userForm.value.middleName,
      this.userForm.value.phoneNumber,
      this.userForm.value.work,
      this.userForm.value.gender,
      this.userForm.value.select,
      this.userForm.value.comment,
      this.userForm.value.skills,
    );

    const next = () => {
      this.requestService.fetchRequests();
    };

    if (this.isEdit) {
      this.requestService.editRequest(request).subscribe(next);
      alert('Данные обновились!');
    } else {
      this.sendPost();
      void this.router.navigate(['']);
    }
  }

  sendPost() {
    const name = this.userForm.value.name;
    const surName = this.userForm.value.surName;
    const middleName = this.userForm.value.middleName;
    const phoneNumber = this.userForm.value.phoneNumber;
    const work = this.userForm.value.work;
    const gender = this.userForm.value.gender;
    const select = this.userForm.value.select;
    const comment = this.userForm.value.comment;
    const skills = this.userForm.value.skills;


    const body = {name, surName, middleName, select, phoneNumber, work, gender, comment, skills};
    this.requestService.sendRequest(body);
    this.register();

  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.userForm.get(fieldName);
    return field && field.touched && field.errors?.[errorType];
  }


  addSkills() {
    const skills = <FormArray>this.userForm.get('skills');
    const skillGroup = new FormGroup({
      skills: new FormControl('', Validators.required),
      levels: new FormControl('', Validators.required),
    })
    skills.push(skillGroup);
  }

  register() {
    void this.router.navigate(['reg']);
  }

  getSkillControls() {
    const skills = <FormArray>this.userForm.get('skills');
    return skills.controls;
  }



}
