import {Component, Input, OnInit} from '@angular/core';
import {Request} from "../../shared/request.model";


@Component({
  selector: 'app-one-request',
  templateUrl: './one-request.component.html',
  styleUrls: ['./one-request.component.css']
})
export class OneRequestComponent implements OnInit {

  @Input() request! : Request;

  constructor() { }

  ngOnInit(): void {
  }


}
