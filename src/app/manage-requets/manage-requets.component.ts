import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {Request} from "../shared/request.model";
import {RequestService} from "../shared/request.service";

@Component({
  selector: 'app-manage-requets',
  templateUrl: './manage-requets.component.html',
  styleUrls: ['./manage-requets.component.css']
})
export class ManageRequetsComponent implements OnInit, OnDestroy{

  requests: Request[]=[];

  requestChangeSubscription!: Subscription;

  constructor(private requestService: RequestService) {}

  ngOnInit() {
    this.requestChangeSubscription = this.requestService.requestChange.subscribe((requests:Request[]) => {

      this.requests = requests;
      console.log(this.requests);
    })
    this.requestService.fetchRequests();
  }


  ngOnDestroy() {
    this.requestChangeSubscription.unsubscribe();
  }

}
