import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ValidateNumberDirective} from "./validate-number.directive";
import {HttpClientModule} from "@angular/common/http";
import {RedirectPageComponent} from "./redirect-page.component";
import { DataComponent } from './data/data.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { ManageRequetsComponent } from './manage-requets/manage-requets.component';
import { OneRequestComponent } from './manage-requets/one-request/one-request.component';
import {RequestService} from "./shared/request.service";
import { RequestDetailsComponent } from './request-details/request-details.component';

@NgModule({
  declarations: [
    AppComponent,
     ValidateNumberDirective,
    RedirectPageComponent,
    DataComponent,
    ToolbarComponent,
    ManageRequetsComponent,
    OneRequestComponent,
    RequestDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [RequestService],
  bootstrap: [AppComponent]
})
export class AppModule { }
