import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RedirectPageComponent} from "./redirect-page.component";
import {DataComponent} from "./data/data.component";
import {ManageRequetsComponent} from "./manage-requets/manage-requets.component";
import {RequestDetailsComponent} from "./request-details/request-details.component";
import {RequestResolverService} from "./request-resolver.service";

const routes: Routes = [
  {path: 'req1', component: DataComponent},
  {path: 'reg', component: RedirectPageComponent},
  {path: 'requests', component: ManageRequetsComponent, children : [
      {path: ':id/edit', component:DataComponent,
        resolve: {
          request: RequestResolverService
        }
      },
      {path: ':id', component: RequestDetailsComponent,
        resolve: {
          request: RequestResolverService
        }
      },


    ]},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
